#include <iostream>

using namespace std;

class Link
{
    public:
    Link* next = NULL;
    virtual bool checkValue(int value){};
    virtual bool compare(Link* node){};
    virtual void output(){};
    virtual Link* copy(){};
    virtual void input(){};
    virtual float sum(){};
    virtual float min(){};
    virtual float geta(int){};
    virtual Link operator ++ (int){};
    float average();
};

template <class Info> class Node: public Link
{
    Info a[3];   
    public:
    bool checkValue(int value);
    bool compare(Link* node);
    void output();
    void input();
    float sum();
    float min();
    Link* copy()
    {   
        cout << "Node has cloned\n";
        return new Node(*this);
    }
    float geta(int i)
    {
        return a[i];
    }
    Link operator ++ (int);
    Node()
    {
        input();
    }
    Node(Node& source_node);

};

class List
{
    Link *last = NULL, *first = NULL;
    public:
    Link* getNode(int index);
    Link* findNode(int value);
    List& operator -- (int);
    void insert(Link* node);
    void clear();
    void output();
    float average();    
    float minimum();
    List operator + (const List& right);
    List& operator = (const List& right);
    bool operator == (const List& right);
    void cloneLast();  
    List(){};
    List(int length);
    List(const List& source_list);
    ~List()
    {
        clear();
    }
};

inline float Link::average()
{
    return sum() / 3;
}

template <class Info> bool Node<Info>::compare(Link* node)
{
    for(int i = 0; i < 3; i++)
        if(node->geta(i) != a[i])
            return false;
    return true;
}

template <class Info> void Node<Info>::output()
{
    int i;
    for(i=0; i<3; i++)
        cout << a[i] << " ";
    cout << "\n";
}

template <class Info> void Node<Info>::input()
{
    cout << "Enter 3 values:\n";
    int i;
    for(i=0; i<3; i++)
        cin >> a[i];    
}

template <class Info> Node<Info>::Node(Node& source_node)
{
    int i;
    for(i=0; i<3; i++)
        a[i] = source_node.a[i];
}

template <class Info> float Node<Info>::sum()
{
    int i;
    float sum = 0;
    for(i=0; i<3; i++)
        sum += a[i];
    return sum;        
}
template <class Info> bool Node<Info>::checkValue(int value)
{
    int i;
    for (i = 0; i < 3; i++)
    {
        if (a[i] == value)
            return true;
    }
    return false;
}
template <class Info> float Node<Info>::min()
{
    float min = a[0];
    int i;
    for (i = 1; i < 3; i++)
        if (a[i] < min)
            min = a[i];
    return min;
}
template <class Info> Link Node<Info>::operator ++ (int)
{
    int i;
    for (i = 0; i < 3; i++)
        a[i] += 1;
    return *this;
}

inline void List::cloneLast()
{
    last->next = last->copy();
    last = last->next;
} 

void List::output()
{
    Link* cur = first;
    for(;cur != NULL; cur = cur->next)
        cur->output();      
}

void List::insert(Link* node)
{
    if (last == NULL)
    {
        last = node;
        first = last;
    }
    else
    {
        last->next = node;
        last = last->next;
    }        
}

Link* List::findNode(int value)
{
    Link* node;
    for (node = first; node != NULL; node = node->next)
    {
        if (node->checkValue(value))
            return node;
    }
    return NULL;
}

Link* List::getNode(int index)
{
    int i = 0;
    Link* node = first;
    for (; node != NULL; i++, node = node->next)
    {
        if (i == index)
        {
            return node;
        }
    }
    cout << "Error: Index out of range\n";
    return NULL;        
}

float List::minimum()
{
    if (first == NULL)
        return -1;
    Link *node = first;
    float min = node->min();
    for (node = node->next; node != NULL; node = node->next)
        if (node->min() < min)
            min = node->min();
    return min;
}

float List::average()
{
    Link *node = first;
    int i = 0;
    float sum = 0;
    for(; node != NULL; i += 3, node = node->next)
        sum += node->sum();
    return sum / i;
}

void List::clear()
{
	Link* node = first, *next;
    if (first != NULL)
        next = first->next;
	for (; node != NULL; node = next)
    {
        next = node->next;
		delete node;
    }
	first = NULL;	
}

List::List(int length)
{
    int type, i;
    Link* newnode;
    for(i = 0; i < length; i++)
    {
        cout << "Node to insert (1.IntNode 2.FloatNode): ";
        cin >> type;
        if (type == 1)
            newnode = new Node<int>;
        else if (type == 2)
            newnode = new Node<float>;
        else
        {
            i--;
            continue;
        }
        this->insert(newnode);
    }
}

List::List(const List& source_list)
{
    Link *node, *source_node = source_list.first;
    first = source_list.first->copy();
    source_node = source_node->next;
    for (node = first; source_node != NULL; source_node = source_node->next, node = node->next)
        node->next = source_node->copy();
    last = node;
}

List& List::operator -- (int)
{
    if (first == NULL)
        return *this;
    Link *node = first;
    if (node == last)
    {
        delete node;
        first = NULL;
    }
    for (; node->next != last; )
        node = node->next;
    delete node->next;
    node->next = NULL;
    last = node;
    cout << "Last Node has been removed\n";
    return *this;
}

List List::operator + (const List& right)
{
    List newlist(*this), append(right);
    newlist.last->next = append.first;
    newlist.last = append.last;
    append.first = NULL;
	return newlist;
}

List& List::operator = (const List& right)
{
	List newlist(right);
	clear();
	this->first = newlist.first;
    this->last = newlist.last;
	newlist.first = NULL;
	return *this;
}

bool List::operator == (const List& right)
{
	Link *node = first, *rightnode = right.first;
	for (; node != NULL; node = node->next, rightnode = rightnode->next)
		if ((node->next == NULL) == !(rightnode->next == NULL))
			return false;
		else
            if(!node->compare(rightnode))
                return false;
	return true;
}

int main()
{
    List demo_list;
    Link* node;
    int input;
    cout << "Enter the length of the List: ";
    cin >> input;
    List list(input);
    while(1)
    {
        cout << "\n1. Add IntNode\n2. Add FloatNode\n3. Output the List\n4. Clone the last Node\n5. Edit Node\n6. List average\n7. Node average\n8. Find Node by value\n9. List minimum\n10. List copying check\n11. List doubling\n12. Remove last Node\n13. Increment Node\n14. Comparation check\n\n0. Exit\n\n";
        cin >> input;
        switch(input)
        {
        case 1:
            list.insert(new Node<int>());
            break;
        case 2:
            list.insert(new Node<float>());
            break;
        case 3:
            cout << "List output:\n";
            list.output();
            break;
        case 4:
            list.cloneLast(); 
            break;
        case 5:
            cout << "Enter the index of Node: ";
            cin >> input;
            node = list.getNode(input);
            if (node)
                node-> input();
            break;
        case 6:
            cout << "List average: " << list.average();
            break;
        case 7:
            cout << "Enter the index of Node: ";
            cin >> input;
            node = list.getNode(input);
            if (node)
                cout << "Node average: " << node->average();
            break;   
        case 8:
            cout << "Enter the Int value: ";
            cin >> input;
            node = list.findNode(input);
            if (node)
            {
                cout << "The value has founded in Node ";
                node->output();
            }
            else
                cout << "The value was not found in the List\n";
            break;
        case 9:
            cout << "List minimum: " << list.minimum();
            break;   
        case 10:
            demo_list = List(list);
            cout << "Demonstration List output:\n";
            demo_list.output();
            break;
        case 11:
            list = list + list;
            cout << "List has been doubled\n";
            break;
        case 12:
            list--;
            break;
        case 13:
            cout << "Enter the index of Node: ";
            cin >> input;
            node = list.getNode(input);
            if (node)
                cout << "Node has been incremented\n";
                (*node)++;
            break; 
        case 14:
            demo_list = list;
            cout << "Cloned List comparation result: " << (demo_list == list) << endl;
            demo_list.cloneLast();
            cout << "Cloned List with cloned last comparation result: " << (demo_list == list) << endl;
            demo_list--;
            cout << "Cloned List without cloned last comparation result: " << (demo_list == list) << endl;
            break;
        case 0:
            return 0;
        }
    }
}