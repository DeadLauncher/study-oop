#include <iostream>

using namespace std;

class Node
{
	float a[3];
public:
	Node* next = NULL;
	void input();
	void output();
	friend float get_node_min_a(Node&);
	float* get_a();
	Node()
	{
		cout << "Enter the new node's data\n";
		input();
	}
	Node(Node* source_node)
	{
		int i;
		for(i=0; i<3; i++)
			a[i] = source_node->a[i];
	}
};

class List
{
public:
	Node* first = NULL;
	void input();
	void output();
	Node* get_node(int number);
	Node* get_a_by_value(float value);
	List(){}
	List(int length);
	List(List& source_list);
	~List()
	{
		Node* node = first;
		for (; node != NULL; node = node->next)
			delete node;		
	}
};

List::List(List& source_list)	
	{
		Node *source_node = source_list.first, *node;
		first = new Node(source_node);
		node = first;
		source_node = source_node->next;
		for (; source_node != NULL; source_node = source_node->next)
		{
			node->next = new Node(source_node);
			node = node->next;
		}
	}

List::List(int length)
	{
		if (length < 1)
			cout << "Invalid length. The list length is set to 0\n";
		else
		{
			int i;
			Node* node = new Node;
			first = node;
			for (i=0; i<length-1; i++)
			{
				node->next = new Node;
				node = node->next;			
			}
		}
	}

float get_min_a(List list)
{
	Node* node = list.first;
	float current, min = get_node_min_a(*node);
	for (node = node->next; node != NULL; node = node->next)
	{
		current = get_node_min_a(*node);
		if (current < min)
			min = current;
	}
	return min;
}

Node* List::get_a_by_value(float value)
{
	float* a;
	int i;
	Node* node = first;
	for (; node != NULL; node = node->next)
	{
		a = node->get_a();
		for(i=0; i<3; i++)
			if (a[i] == value)
				return node;
	}
	return NULL;
}

Node* List::get_node(int number)
{
	Node* node = first;
	int i;
	for (i = 0; i < number - 1; i++)
	{
		node = node->next;
		if (node == NULL)
			break;
	}
	return node;
}

float get_node_min_a(Node& node)
{
	int i;
	float min = node.a[0];
	for (i=1; i<3; i++)
	{
		if (node.a[i] < min)
			min = node.a[i];
	}
	return min;
}

inline float* Node::get_a()
{
	return a;
}

inline void Node::input()
{
	int i;
	for (i = 0; i < 3; i++)
		cin >> a[i];
}

inline void Node::output()
{
	int i;
	for (i = 0; i < 3; i++)
		cout << a[i] << " ";
	cout << "\n";
}

inline void List::input()
{
	int count = 0;
	float summ = 0;
	Node* node = first;
	for (; node != NULL; node = node->next)
		node->input();
}

inline void List::output()
{
	int count = 0;
	float summ = 0;
	Node* node = first;
	for (; node != NULL; node = node->next)
		node->output();
	cout << "\n";
}

int main()
{
	float *a, val;
	List *list, *lists[] = {NULL, NULL, NULL};
	Node *node;
	int i, input;
	while (true)
	{		
		cout << "1 - Create list with length\n2 - Create list from another list\n3 - List operations\n\n0 - Exit\n";
		cin >> input;
		switch(input)
		{
		case 0:
			for (i=0; i<3; i++)
				if (lists[i] != NULL)
					delete lists[i];
			return 0;
		case 1:
			cout << "Choose list to replace (1-3)\n";
			cin >> i;
			if (i > 0 && i < 4)
			{
				if (lists[i-1] != NULL)
					delete lists[i-1];
				cout << "Set list's length\n";
				cin >> input;	
				lists[i-1] = new List(input);
				cout << "List number " << i << " created successfully\n";
			}
			else
				cout << "Wrong number\n";
			break;
		case 2:
			cout << "Choose the number of the source list (1-3)\n";
			cin >> input;
			if (lists[input-1] == NULL)
			{
				cout << "List is not created\n";
				break;
			}
			cout << "Choose the number of the new list (except " << input << ")\n";
			cin >> i;
			if (i == input)
			{
				cout << "You can not replace the source list\n";
				break;
			}
			lists[i-1] = new List(*lists[input-1]);
			cout << "List " << input << " successfully copied to " << i << "\n";
			break;
		case 3:
			cout << "Enter list number (1-3)\n";
			cin >> input;
			if (input > 0 && input < 4)
			{
				if (lists[input-1] != NULL)
					list = lists[input - 1];
				else
				{
					cout << "List is not created\n";
					break;
				}
			}
			else
			{
				cout << "Invalid number\n";
				break;	
			}					
			cout << "1 - Output\n2 - Input\n3 - List minimum\n4 - Find value\n\n0 - Back\n";
			cin >> input;
			switch (input)
			{
			case 1:
				cout << "1 - By node\n2 - by list\n\n0 - Back\n";
				cin >> input;
				switch (input)
				{
				case 1:
					cout << "Enter the number of node to output:\n";
					cin >> input;
					node = list->get_node(input);
					if (node)
					{
						cout << "Node " << input << " data:\n";
						node->output();
					}
					else
						cout << "Error: node doesn't exist.\n";
					break;
				case 2:
					list->output();
				}
				break;
			case 2:
				cout << "1 - By node\n2 - By list\n\n0 - Back\n";
				cin >> input;
				switch (input)
				{
				case 1:
					cout << "Enter the number of node to input:\n";
					cin >> input;
					node = list->get_node(input);
					if (node)
					{
						cout << "Enter the node data " << input << ":\n";
						node->input();
					}
					else
						cout << "Error: node doesn't exist.\n";
					break;
				case 2:
					list->input();
				}
				break;
			case 3:
				cout << "Minimum of the list: " << get_min_a(*list) << "\n";
				break;
			case 4:
				cout << "Enter a value: ";
				cin >> val;
				node = list->get_a_by_value(val);
				if (node != NULL)
				{
					cout << "The value was found in node ";
					node->output();
				}
				else
					cout << "The value was not found in the list\n";
			}
		}
	}
	return 0;
}