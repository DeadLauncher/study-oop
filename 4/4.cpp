#include <iostream>

using namespace std;

class Node
{
    public:
    Node* next = NULL;
    int length = 0;
    virtual Node operator ++ (int){};
    virtual void output(){};
    virtual Node* copy(){};
    virtual void input(){};
    virtual float sum(){};
    virtual bool checkValue(int value){};
    virtual float min(){};
    virtual bool compare(Node* node){};
    float average()
    {
        if (length > 0)
            return sum() / length;
    }
};

class IntNode: public Node
{
    int b;
    public:
    bool compare(Node* node);
    void output()
    {
        cout << b << endl;
    }
    void input()
    {
        cout << "Enter int value: ";
        cin >> b;
        length = 1;
    }
    Node* copy()
    {
        cout << "IntNode has cloned\n";
        return new IntNode(*this);
    }
    float sum()
    {
        return b;
    }
    bool checkValue(int value)
    {
        return (float) b == value;
    }
    float min()
    {
        return b;
    }
    Node operator ++ (int)
    {
        b++;
        return *this;
    }
    IntNode() 
    {
        input();
    };
    IntNode(IntNode& source_node)
    {
        b = source_node.b;
        length = 1;
    }

};

class FloatNode: public Node
{
    float a[3];   
    public:
    void output();
    void input();
    float sum();
    bool checkValue(int value);
    bool compare(Node* node);
    float min();
    Node operator ++ (int);
    Node* copy()
    {   
        cout << "FloatNode has cloned\n";
        return new FloatNode(*this);
    }
    FloatNode() 
    {
        input();
    }
    FloatNode(FloatNode& source_node);

};

class List
{
    Node *last = NULL, *first = NULL;
    public:
    List operator -- (int);
    List operator + (const List& right);
    List operator = (const List& right);
    bool operator == (const List& right);
    void clear();
    void insert(Node* node);
    void output();
    Node* getNode(int index);
    Node* findNode(int value);
    float average();    
    float minimum();
    void cloneLast()
    {
        last->next = last->copy();
        last = last->next;
    }   
    List(){};
    List(int length);
    List(const List& source_list);
    ~List()
    {
        clear();
    }
};

bool IntNode::compare(Node* node)
{
    if(node->length == length)
        if(((IntNode*)node)->b == b)
            return true;
    return false;
}

bool FloatNode::compare(Node* node)
{
    if(node->length == length)
    {
        float* a2 = ((FloatNode*)node)->a;
        for(int i = 0; i < 3; i++)
            if(a2[i] != a[i])
                return false;
        return true;
    }
    return false;
}

void FloatNode::output()
{
    int i;
    for(i=0; i<3; i++)
        cout << a[i] << " ";
    cout << "\n";
}

void FloatNode::input()
{
    cout << "Enter 3 float values:\n";
    int i;
    for(i=0; i<3; i++)
        cin >> a[i]; 
    length = 3;      
}

FloatNode::FloatNode(FloatNode& source_node)
{
    int i;
    for(i=0; i<3; i++)
        a[i] = source_node.a[i];
    length = 3;
}

float FloatNode::sum()
{
    int i;
    float sum = 0;
    for(i=0; i<3; i++)
        sum += a[i];
    return sum;        
}
bool FloatNode::checkValue(int value)
{
    int i;
    for (i = 0; i < 3; i++)
    {
        if (a[i] == value)
            return true;
    }
    return false;
}
float FloatNode::min()
{
    float min = a[0];
    int i;
    for (i = 1; i < 3; i++)
        if (a[i] < min)
            min = a[i];
    return min;
}
Node FloatNode::operator ++ (int)
{
    int i;
    for (i = 0; i < 3; i++)
        a[i] += 1;
    return *this;
}

void List::output()
{
    Node* cur = first;
    for(;cur != NULL; cur = cur->next)
        cur->output();        
}

void List::insert(Node* node)
{
    if (last == NULL)
    {
        last = node;
        first = last;
    }
    else
    {
        last->next = node;
        last = last->next;
    }        
}

Node* List::findNode(int value)
{
    Node* node;
    for (node = first; node != NULL; node = node->next)
    {
        if (node->checkValue(value))
            return node;
    }
    return NULL;
}

Node* List::getNode(int index)
{
    int i = 0;
    Node* node = first;
    for (; node != NULL; i++, node = node->next)
    {
        if (i == index)
        {
            return node;
        }
    }
    cout << "Error: Index out of range\n";
    return NULL;        
}

float List::minimum()
{
    if (first == NULL)
        return -1;
    Node *node = first;
    float min = node->min();
    for (node = node->next; node != NULL; node = node->next)
        if (node->min() < min)
            min = node->min();
    return min;
}

float List::average()
{
    Node *node = first;
    int i = 0;
    float sum = 0;
    for(; node != NULL; i += node->length, node = node->next)
        sum += node->sum();
    return sum / i;
}

void List::clear()
{
	Node* node = first, *next;
    if (first != NULL)
        next = first->next;
	for (; node != NULL; node = next)
    {
        next = node->next;
		delete node;
    }
	first = NULL;	
}

List::List(int length)
{
    int type, i;
    Node* newnode;
    for(i = 0; i < length; i++)
    {
        cout << "Node to insert (1.IntNode 2.FloatNode): ";
        cin >> type;
        if (type == 1)
            newnode = new IntNode;
        else if (type == 2)
            newnode = new FloatNode;
        else
        {
            i--;
            continue;
        }
        this->insert(newnode);
    }
}

List::List(const List& source_list)
{
    Node *node, *source_node = source_list.first;
    first = source_list.first->copy();
    source_node = source_node->next;
    for (node = first; source_node != NULL; source_node = source_node->next, node = node->next)
        node->next = source_node->copy();
    last = node;
}

List List::operator -- (int)
{
    if (first == NULL)
        return *this;
    Node *node = first;
    if (node == last)
    {
        delete node;
        first = NULL;
    }
    for (; node->next != last; )
        node = node->next;
    delete node->next;
    node->next = NULL;
    last = node;
    cout << "Last Node has been removed\n";
    return *this;
}

List List::operator + (const List& right)
{
    List newlist(*this), append(right);
    newlist.last->next = append.first;
    newlist.last = append.last;
    append.first = NULL;
	return newlist;
}

List List::operator = (const List& right)
{
	List newlist(right);
	clear();
	this->first = newlist.first;
    this->last = newlist.last;
	newlist.first = NULL;
	return *this;
}

bool List::operator == (const List& right)
{
	Node *node = first, *rightnode = right.first;
	for (; node != NULL; node = node->next, rightnode = rightnode->next)
		if ((node->next == NULL) == !(rightnode->next == NULL))
			return false;
		else
            if(!node->compare(rightnode))
                return false;
	return true;
}

int main()
{
    List demo_list;
    Node* node;
    int input;
    cout << "Enter the length of the List: ";
    cin >> input;
    List list(input);
    while(1)
    {
        cout << "\n1. Add IntNode\n2. Add FloatNode\n3. Output the List\n4. Clone the last Node\n5. Edit Node\n6. List average\n7. Node average\n8. Find Node by value\n9. List minimum\n10. List copying check\n11. List doubling\n12. Remove last Node\n13. Increment Node\n14. Comparation check\n\n0. Exit\n\n";
        cin >> input;
        switch(input)
        {
        case 1:
            list.insert(new IntNode());
            break;
        case 2:
            list.insert(new FloatNode());
            break;
        case 3:
            cout << "List output:\n";
            list.output();
            break;
        case 4:
            list.cloneLast(); 
            break;
        case 5:
            cout << "Enter the index of Node: ";
            cin >> input;
            node = list.getNode(input);
            if (node)
                node-> input();
            break;
        case 6:
            cout << "List average: " << list.average();
            break;
        case 7:
            cout << "Enter the index of Node: ";
            cin >> input;
            node = list.getNode(input);
            if (node)
                cout << "Node average: " << node->average();
            break;   
        case 8:
            cout << "Enter the Int value: ";
            cin >> input;
            node = list.findNode(input);
            if (node)
            {
                cout << "The value has founded in Node ";
                node->output();
            }
            else
                cout << "The value was not found in the List\n";
            break;
        case 9:
            cout << "List minimum: " << list.minimum();
            break;   
        case 10:
            demo_list = List(list);
            cout << "Demonstration List output:\n";
            demo_list.output();
            break;
        case 11:
            list = list + list;
            cout << "List has been doubled\n";
            break;
        case 12:
            list--;
            break;
        case 13:
            cout << "Enter the index of Node: ";
            cin >> input;
            node = list.getNode(input);
            if (node)
                cout << "Node has been incremented\n";
                (*node)++;
            break; 
        case 14:
            demo_list = list;
            cout << "Cloned List comparation result: " << (demo_list == list) << endl;
            demo_list.cloneLast();
            cout << "Cloned List with cloned last comparation result: " << (demo_list == list) << endl;
            demo_list--;
            cout << "Cloned List without cloned last comparation result: " << (demo_list == list) << endl;
            break;
        case 0:
            return 0;
        }
    }
}