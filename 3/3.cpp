#include <iostream>

using namespace std;

class Node
{
	float a[3];
public:
	Node* next = NULL;
	float* get_a();
    friend Node operator ++ (Node& node, int);
	friend ostream& operator << (ostream&, const Node&);
	friend istream& operator >> (istream& in, Node& node);
	Node()
	{
		cout << "Enter the new node's data\n";
		cin >> *this;
	}
	Node(Node* source_node)
	{
		int i;
		for(i=0; i<3; i++)
			a[i] = source_node->a[i];
	}
};

class List
{
public:
    List operator + (const List&);
	List operator = (const List&);
	bool operator == (const List&);
	friend List operator -- (List&, int);
	Node* first = NULL;
	Node* get_node(int number);
	void clear();
	List(){}
	List(int length);
	List(const List* source_list);
	~List()
	{
		clear();
	}
};

List::List(int length)
	{
		if (length < 1)
			cout << "Invalid length. The list length is set to 0\n";
		else
		{
			cout << "Fill the list\n";
			int i;
			Node* node = new Node;
			first = node;
			for (i=0; i<length-1; i++)
			{
				node->next = new Node;
				node = node->next;			
			}
		}
	}

List::List(const List& source_list)
	{
		Node *source_node = source_list.first, *node;
		first = new Node(source_node);
		node = first;
		source_node = source_node->next;
		for (; source_node != NULL; source_node = source_node->next)
		{
			node->next = new Node(source_node);
			node = node->next;
		}
	}

List List::operator = (const List& right)
{
	List newlist(right);
	clear();
	this->first = newlist.first;
	newlist.first = NULL;
	return *this;
}

List List::operator + (const List& right)
{
	List newlist;
	Node *node, *source_node = first;
	int i = 0;
	newlist.first = new Node(*source_node);
	node = newlist.first;
	source_node = source_node->next;
	while(i < 2)
	{
		for(; source_node != NULL; source_node = source_node->next)
		{
			node->next = new Node(source_node);
			node = node->next;
		}
		source_node = right.first;
		i++;
	}
	return newlist;
}

bool List:: operator == (const List& right)
{
	float *a1, *a2;
	int i;
	Node *node = first, *rightnode = right.first;
	for (; node != NULL; node = node->next, rightnode = rightnode->next)
		if ((node->next == NULL) == !(rightnode->next == NULL))
			return false;
		else
		{
			a1 = node->get_a();
			a2 = rightnode->get_a();
			for(i=0; i<3; i++)
			{
				if (a1[i] != a2[i])
					return false;
			}
		}
	return true;
}

List operator -- (List& list, int)
{
	if (list.first == NULL)
		return list;
	Node *node = list.first;
	if (node->next == NULL)
	{
		delete node;
		list.first = NULL;
	}
	for (; node->next->next != NULL; )
		node = node->next;
	delete node->next;
	node->next = NULL;
	return list;
}

inline Node operator ++ (Node& node, int)
{
    int i = 0;
    for(;i<3; i++)
        node.a[i] += 1.0;
	return node;
}

void List::clear()
{
	Node* node = first;
	for (; node != NULL; node = node->next)
		delete node;
	first = NULL;	
} 

Node* List::get_node(int number)
{
	Node* node = first;
	int i;
	for (i = 0; i < number - 1; i++)
	{
		node = node->next;
		if (node == NULL)
			break;
	}
	return node;
}

inline float* Node::get_a()
{
	return a;
}

ostream& operator << (ostream& out, const Node& node)
{
    int i;
	for(i=0; i<3; i++)
		out << node.a[i] << " ";
	out << "\n";
    return out;
}

istream& operator >> (istream& in, Node& node)
{
    int i;
	for(i=0; i<3; i++)
		in >> node.a[i];
    return in;
}

ostream& operator << (ostream& out, const List& list)
{
	Node *node = list.first;
	for(; node != NULL; node = node->next)
		out << *node;
    return out;
}

istream& operator >> (istream& in, List& list)
{
	Node *node = list.first;
	for(; node != NULL; node = node->next)
		in >> *node;
    return in;
}

int main()
{
	Node node;
	node++;
	cout << "node++\n" << node;
	List list1(2), list2(3);
	List list3 = list1 + list2;
	cout << "list3 = list1 + list2:\n" << list3;
	list3 = list2 = list1;
	cout << "list1:\n" << list1 << "list2:\n" << list2 << "list3:\n" << list3;
	cout << "Input list3:\n";
	cin >> list3;
	cout << "list1 == list2: " << (list1 == list2) << "\n";
	cout << "list2 == list3: " << (list2 == list3) << "\n";
	cout << "list2--:\n" << list2--;
    return 0;
}