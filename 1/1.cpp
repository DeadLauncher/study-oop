#include <iostream>

class Node
{
public:
	Node* next = NULL;
	float a[3];
	float summ();
	void input();
	void output();
};

class List
{
public:
	Node* first = NULL;
	float average();
	void input();
	void output();
	Node* get_node(int number);
};

Node* List::get_node(int number)
{
	Node* node = first;
	int i;
	for (i = 0; i < number - 1; i++)
	{
		node = node->next;
		if (node == NULL)
			break;
	}
	return node;
}

float Node::summ()
{
	int i;
	float summ = 0;
	for (i = 0; i < 3; i++)
		summ += a[i];
	return summ;
}

void Node::input()
{
	int i;
	for (i = 0; i < 3; i++)
		std::cin >> a[i];
}

void Node::output()
{
	int i;
	for (i = 0; i < 3; i++)
		std::cout << a[i] << " ";
	std::cout << "\n";
}

float List::average()
{
	int count = 0;
	float summ = 0;
	Node* node = first;
	for (; node != NULL; node = node->next)
	{
		summ += node->summ();
		count += 3;
	}
	return summ / count * 1.0;
}

void List::input()
{
	int count = 0;
	float summ = 0;
	Node* node = first;
	for (; node != NULL; node = node->next)
		node->input();
}

void List::output()
{
	int count = 0;
	float summ = 0;
	Node* node = first;
	for (; node != NULL; node = node->next)
		node->output();
	std::cout << "\n";
}

int main()
{
	List list;
	Node* node = new Node;
	list.first = node;
	int i, input;
	while (true)
	{
		std::cout << "Enter node data\n";
		node->input();
		std::cout << "Continue input? (1 - yes)\n";
		std::cin >> input;
		if (input != 1)
			break;
		node->next = new Node;
		node = node->next;
	}
	while (true)
	{
		std::cout << "1 - Output\n2 - Input\n3 - List average\n4 - Node summ\n\n0 - Exit\n";
		std::cin >> input;
		switch (input)
		{
		case 0:
			return 0;
		case 1:
			std::cout << "1 - By node\n2 - by list\n\n0 - Back\n";
			std::cin >> input;
			switch (input)
			{
			case 1:
				std::cout << "Enter the number of node to output:\n";
				std::cin >> input;
				node = list.get_node(input);
				if (node)
				{
					std::cout << "Node " << input << "data:\n";
					node->output();
				}
				else
					std::cout << "Error: node doesn't exist.\n";
				break;
			case 2:
				list.output();
			}
			break;
		case 2:
			std::cout << "1 - By node\n2 - By list\n\n0 - Back\n";
			std::cin >> input;
			switch (input)
			{
			case 1:
				std::cout << "Enter the number of node to input:\n";
				std::cin >> input;
				node = list.get_node(input);
				if (node)
				{
					std::cout << "Enter the node data " << input << ":\n";
					node->input();
				}
				else
					std::cout << "Error: node doesn't exist.\n";
				break;
			case 2:
				list.input();
			}
			break;
		case 3:
			std::cout << "Average:\n" << list.average() << std::endl;
			break;
		case 4:
			std::cout << "Enter the number of node to output:\n";
			std::cin >> input;
			node = list.get_node(input);
			if (node)
			{
				std::cout << "Node " << input << " summ: " << node->summ() << "\n";
			}
			else
				std::cout << "Error: node doesn't exist.\n";
		}
	}

	return 0;
}