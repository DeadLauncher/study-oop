#include <iostream>
#include <fstream>

using namespace std;

//класс абстракных матриц
class Matrix
{
    public:
    char* matrix;
    int size = 0;
    int bytesize;

    void input(istream& in);
    void output(ostream& out);
    void filein(const char* filename);
    void fileout(const char* filename);
    char* matrix_copy();
    void element_edit(int row, int col);

    virtual void element_in(istream& in, char* element){};
    virtual void element_out(ostream& out, char* element){};
    virtual float maximum(){};
    virtual void writeMin(Matrix&, int){};

    bool operator>(Matrix& right);
    Matrix& operator/=(Matrix& right);

    friend ostream& operator << (ostream&, Matrix&);
	friend istream& operator >> (istream&, Matrix&);

    Matrix& operator=(Matrix&);

    Matrix(){}
    Matrix(Matrix& source);
    //деструктор матрицы
    ~Matrix()
    {
        delete matrix;
    }

};

//ввод матрицы целиком из потока in
void Matrix::input(istream& in)
{
    for(int i=0; i<size; i++)
    {
        cout << "Enter the row:\n";
        for(int j=0; j<size; j++)
            element_in(in, matrix + i * bytesize * size + j * bytesize);
    }
}

//вывод матрицы целиком в поток out
void Matrix::output(ostream& out)
{
    for(int i=0; i<size; i++)
    {
        for(int j=0; j<size; j++)
            element_out(out, matrix + i * bytesize * size + j * bytesize);
        out << "\n";
    }
}

//ввод матрицы целиком из файла filename
void Matrix::filein(const char* filename)
{
    ifstream file(filename);
    input(file);
    file.close();
}

//вывод матрицы целиком в файл filename
void Matrix::fileout(const char* filename)
{
    ofstream file(filename);
    output(file);
    file.close();
}

//ввод значения элемента матрицы на позиции (row, col)
void Matrix::element_edit(int row, int col)
{
    element_in(cin, matrix + row * size * bytesize + col * bytesize);
}

//физическое копирование массива матрицы
char* Matrix::matrix_copy()
{
    char* newmatrix = new char[size * size * bytesize];
    for(int i=0; i<size*size*bytesize; i++)
        newmatrix[i] = matrix[i];
    return newmatrix;
}

//перегрузка оператора потокового вывода
ostream& operator << (ostream& out, Matrix& matrix)
{
    matrix.output(out);
    return out;
}

//перегрузка оператора потокового ввода
istream& operator >> (istream& in, Matrix& matrix)
{
    matrix.input(in);
    return in;
}

//перегрузка оператора > для матриц
bool Matrix::operator>(Matrix& right)
{
    return maximum() > right.maximum();
}

//перегрузка оператора /= для матриц
Matrix& Matrix::operator/=(Matrix& right)
{
    if (size != right.size || bytesize != right.bytesize)
        return *this;
    for (int i = 0; i < size * size; i++)
        writeMin(right, i);
    return *this;
}

//конструктор копирования матрицы
Matrix::Matrix(Matrix& source)
{
    size = source.size;
    bytesize = source.bytesize;
    matrix = source.matrix_copy();
}

//перегрузка оператора присвоения
Matrix& Matrix::operator=(Matrix& right)
{
    size = right.size;
    bytesize = right.bytesize;
    delete matrix;
    matrix = right.matrix_copy();
    return *this;
}

//класс матриц вещественных числей
class FloatMatrix: public Matrix
{
    public:
    //ввод элемента по адресу element из потока in
    void element_in(istream& in, char* element)
    {
        in >> *((float*) element);
    }
    //вывод элемента по адресу element в поток out
    void element_out(ostream& out, char* element)
    {
        out << *((float*) element) << " ";
    }
    float maximum();
    void writeMin(Matrix& m2, int i);
    FloatMatrix(){}
    FloatMatrix(int n);
};

//запись в исходную матрицу минимального из двух матриц элемента на позиции i
void FloatMatrix::writeMin(Matrix& m2, int i)
{
    if ( *(float*)(matrix + i * bytesize) < *(float*)(m2.matrix + i * m2.bytesize))
            *(float*)(matrix + i * bytesize) = *(float*)(m2.matrix + i * m2.bytesize);  
}

//возврат наибольшего значения в матрице
float FloatMatrix::maximum()
{
    float max = *(float*)matrix;
    for(int i = 1; i < size * size; i++)
        if (*(float*)(matrix + i * bytesize) > max)
            max = *(float*)(matrix + i * bytesize);
    return max;
}

//конструктор вещественной матрицы заданного размера n
FloatMatrix::FloatMatrix(int n)
{
    bytesize = sizeof(float);
    size = n;
    matrix = new char[size * size * bytesize];
    input(cin);
}

int main()
{
    int input, row, col;
    cout << "Enter the size of new matrices: ";
    cin >> input;
    FloatMatrix m1(input), m2(input);
    do
    {
        cout << "1. Matrix output\n2. Matrix element edit\n3. Copy Matrix1 to Matrix2\n4. Matrix file input\n5. Matrix file output\n6. Matrix1 /= Matrix2\n7. Matrix1 > Matrix2\n\n0. Exit\n";
        cin >> input;
        switch(input)
        {
            case 1:
                cout << "m1:\n" << m1 << "\nm2:\n" << m2;
                break;
            case 2:
                cout << "Which matrix: ";
                cin >> input;
                cout << "Enter row and column: ";
                cin >> row >> col;
                if (input == 1)
                    m1.element_edit(row, col);
                else
                    m2.element_edit(row, col);
                break;
            case 3:
                cout << "Copied\n";
                m2 = m1;
                break;
            case 4:
                cout << "Which matrix: ";
                cin >> input;
                if (input == 1)
                    m1.filein("out");
                else
                    m2.filein("out");
                cout << "Opened\n";
                break;
            case 5:
                cout << "Which matrix: ";
                cin >> input;
                if (input == 1)
                    m1.fileout("out");
                else
                    m2.fileout("out");
                cout << "Saved\n";
                break;
            case 6:
                cout << "m1 /= m2:\n" << (m1 /= m2);
                break;
            case 7:
                cout << "m1 > m2:\n" << (m1 > m2);
        }
    } while (input);
    return 0;
}